########## Importer les modules necessaires ##############
from tkinter import *
from random import *
import time
from tkinter.font import Font
import tkinter as tk

##########################################################
##########    Variables ##################################
##########################################################
J1_rue = [] #propriete de J1
J2_rue = [] #propriete de J2
nb_caseJ1 = 1 #situe les joueurs celon la case sur laquelle ils se trouvent, de 1 à 40
nb_caseJ2 = 1 #situe les joueurs celon la case sur laquelle ils se trouvent, de 1 à 40
J1_argent = 1500
J2_argent = 1500
y1=120  #Pour les pôsitions des messages
y2=120  #Pour les pôsitions des messages
messageJ1=[]
messageJ2=[]
nb_tours=0
tours_prison=0
y_prop=385 #pour les positions des propriÃ©tÃ©s
y_prop2=385
parc_free=150
doubleJ1 = False 
doubleJ2 = False 
compt_doubleJ1 = 0
compt_doubleJ2 = 0
prison = False
dé = 0
reponse = ''
t = 0
pbt1_dés = 53
pbt1_oui = 140
pbt1_non = 10
pbt2_dés = 1070
pbt2_oui = 1030
pbt2_non = 1160

# struc rue: [nom rue,nb_case,rue(True si c une rue false sinon),loyer,prix,hypotheque,racha,acheter ou non(0(personne)_1(J1)_2(J2))]
plateau = [['case depart',1,False,None,None,None,None,None],
           ['Boulevard de Belleville',2,True,2,60,30,33,0], #listes des rues 
           ['chance',3,False,None,None,None,None,None],
           ['Rue Lecourbe',4,True,4,60,30,33,0],
           ['Impots sur le revenu',5,False,None,None,None,None,None],
           ['Gare Montparnasse',6,True,None,200,100,110,0],
           ['Rue de Vaugirard',7,True,6,100,50,55,0],
           ['Chance',8,False,None,None,None,None,None],
           ['Rue de Courcelles',9,True,6,100,50,55,0],
           ['Avenue de la République',10,True,8,120,60,66,0],
           ['Prison',11,False,None,None,None,None,None],
           ['Boulevard de la Villette',12,True,10,140,70,77,0],
           ["Compagnie de distribution d'électricité",13,True,None,150,75,83,0],
           ['Avenue de Neuilly',14,True,10,140,70,77,0],
           ['Rue de Paradis',15,True,12,160,80,88,0],
           ['Gare de Lyon',16,True,None,200,100,110,0],
           ['Avenue Mozart',17,True,14,180,90,99,0],
           ['Chance',18,False,None,None,None,None,None],
           ['Boulevard Saint-Michel',19,True,14,180,90,99,0],
           ['Place Pigalle',20,True,16,200,100,110,0],
           ['Parc gratuit',21,False,None,None,None,None,None],
           ['Avenue Matignon',22,True,18,220,110,121,0],
           ['Chance',23,False,None,None,None,None,None],
           ['Boulevard Malesherbes',24,True,18,220,110,121,0],
           ['Avenue Henri-Martin',25,True,20,240,120,132,0],
           ['Gare du Nord',26,True,None,200,100,110,0],
           ['Faubourg Saint-Honoré',27,True,22,260,130,143,0],
           ['Place de la Bourse',28,True,22,260,130,143,0],
           ["Compagnie de distribution des eaux",29,True,None,150,75,83,0], 
           ['Rue La Fayette',30,True,24,280,140,154,0],
           ['Allez en prison',31,False,None,None,None,None,None],
           ['Avenue de Breteuil',32,True,26,300,150,165,0],
           ['Avenue Foch',33,True,26,300,150,165,0],
           ['Chance',34,False,None,None,None,None,None],
           ['Boulevard des Capucines',35,True,28,320,160,176,0],
           ['Gare Saint-Lazare',36,True,None,200,100,110,0],
           ['Chance',37,False,None,None,None,None,None],
           ['Avenue des Champs-Élysées',38,True,35,350,175,193,0],
           ['Taxe de luxe',39,False,None,None,None,None,None],
           ['Rue de la Paix',40,True,50,400,200,220,0]]

rue = [2,4,6,7,9,10,12,13,14,15,16,17,19,20,22,24,25,26,27,28,29,30,32,33,35,36,38,40]#nb_case qui coorespondent a des rues
chance = [3,8,18,23,34,37]

carte_chance = [['allez à la gare Montparnasse. Si vous passez par la case départ, recevez 200M'],
                ['Rendez-vous à l’Avenue Henri-Martin. Si vous passez par la case départ, recevez 200M'],
                ['Avancez au Boulevard de La Villette. Si vous passez par la case départ, recevez 200M'],
                ['Avancez jusqu’à la Gare de Lyon. Si vous passez par la case départ, recevez 200M'],
                ['Reculez de trois cases'],
                ['Aller en prison. Rendez-vous directement en prison. Ne franchissez pas par la case départ, ne touchez pas 200M'],
                ['Amende pour excès de vitesse payez 15M'],
                ['Amende pour ivresse payez 25M'],
                ['Votre immeuble et votre prêt rapportent. Vous recevez 150M'],
                ['Allez a la case départ, recevez 200M'],
                ['Erreur de la banque en votre faveur. Recevez 200M'],
                ['Payez la note du médecin 50M'],
                ['La vente de votre stock vous rapporte 50M'],
                ['C’est votre anniversaire. Chaque joueur doit vous donner 25M'],
                ['Payez votre Police d’Assurance 50M'],
                ['Vous avez gagné le deuxième Prix de Beauté. Recevez 10M'],
                ['Vous héritez de 100M'],
                ["Avancez jusqu'a la rue de la paix"],
                ['Avancez jusqu’au Boulevard de la Villette. Si vous passez par la case départ, recevez 200M'],
                ['Vous avez été élu president du conseil d’administration. Payez à chaque joueur 50M']]

##########################################################
##########    Variables ##################################
##########################################################

def chance_actionJ1(carte_chance):
    '''
    accomplie les action des cartes chances tirées
    '''
    global J1_rue
    global nb_caseJ1
    global J1_argent
    global J2_argent
    global rue
    global chance
    carte_tiré = carte_chance
    if carte_tiré==0: #allez à la gare Montparnasse
        if nb_caseJ1>6:
            J1_argent+=200
            nb_caseJ1 = 6
        else:
            nb_caseJ1 = 6
    elif carte_tiré==1: #Rendez-vous à l’Avenue Henri-Martin
        if nb_caseJ1>25:
            J1_argent+=200
            nb_caseJ1 = 25
        else:
            nb_caseJ1 = 25
    elif carte_tiré==2: #Avancez au Boulevard de La Villette
        if nb_caseJ1>14:
            J1_argent+=200
            nb_caseJ1 = 14
        else:
            nb_caseJ1 = 14
    elif carte_tiré==3: #Avancez jusqu’à la Gare de Lyon
        if nb_caseJ1>16:
            J1_argent+=200
            nb_caseJ1 = 16
        else:
            nb_caseJ1 = 16
    elif carte_tiré==4: #Reculez de trois cases
        nb_caseJ1-=3
    elif carte_tiré==5: #Aller en prison. Ne franchissez pas par la case départ, ne touchez pas 200M
        nb_caseJ1 = 31
    elif carte_tiré==6: #Amende pour excès de vitesse payez 15M
        J1_argent-=15
    elif carte_tiré==7: #Amende pour ivresse payez 25M
        J1_argent-=25
    elif carte_tiré==8: #Votre immeuble et votre prêt rapportent. Vous recevez 150M:
        J1_argent+=150
    elif carte_tiré==9: #Allez a la case départ, recevez 200M
        nb_caseJ1 = 1
        J1_argent+=200
    elif carte_tiré==10: #Erreur de la banque en votre faveur. Recevez 200M:
        J1_argent+=200
    elif carte_tiré==11: #Payez la note du médecin 50M
        J1_argent-=50
    elif carte_tiré==12: #La vente de votre stock vous rapporte 50M'
        J1_argent+=50
    elif carte_tiré==13: #C’est votre anniversaire. Chaque joueur doit vous donner 25M
        J1_argent+=25
        J2_argent-=25
    elif carte_tiré==14: #Payez votre Police d’Assurance 50M
        J1_argent-=50
    elif carte_tiré==15: #Vous avez gagné le deuxième Prix de Beauté. Recevez 10M
        J1_argent+=10
    elif carte_tiré==16: #Vous héritez de 100M
        J1_argent+=100
    elif carte_tiré==17: #Avancez jusqu'a la rue de la paix
        nb_caseJ1 = 40
    elif carte_tiré==18: #Avancez jusqu’au Boulevard de la Villette. Si vous passez par la case départ, recevez 200M
        if nb_caseJ1>12:
            J1_argent+=200
            nb_caseJ1 = 12
        else:
            nb_caseJ1 = 12
    elif carte_tiré==19: #Vous avez été élu president du conseil d’administration. Payez à chaque joueur 50M
        J1_argent-=50
        J2_argent+=50    
    affiche_argentJ1(J1_argent)
    affiche_argentJ2(J2_argent)
    affichage_pionJ1(nb_caseJ1)
    affich_parc(parc_free)
    roc = rue_or_chance1(nb_caseJ1,rue,chance)
    if roc==1:
       affiche_messJ1('voulez-vous acheter ?')
       bouton1["state"] = tk.DISABLED
       oui1["state"] = tk.NORMAL
       non1["state"] = tk.NORMAL   

def chance_actionJ2(carte_chance):
    '''
    accomplie les action des cartes chances tirées
    '''
    global J2_rue
    global nb_caseJ2
    global J1_argent
    global J2_argent
    carte_tiré = carte_chance
    if carte_tiré==0: #allez à la gare Montparnasse
        if nb_caseJ2>6:
            J2_argent+=200
            nb_caseJ2 = 6
        else:
            nb_caseJ2 = 6
    elif carte_tiré==1: #Rendez-vous à l’Avenue Henri-Martin
        if nb_caseJ2>25:
            J2_argent+=200
            nb_caseJ2 = 25
        else:
            nb_caseJ2 = 25
    elif carte_tiré==2: #Avancez au Boulevard de La Villette
        if nb_caseJ2>14:
            J2_argent+=200
            nb_caseJ2 = 14
        else:
            nb_caseJ2 = 14
    elif carte_tiré==3: #Avancez jusqu’à la Gare de Lyon
        if nb_caseJ2>16:
            J2_argent+=200
            nb_caseJ2 = 16
        else:
            nb_caseJ2 = 16
    elif carte_tiré==4: #Reculez de trois cases
        nb_caseJ2-=3
    elif carte_tiré==5: #Aller en prison. Ne franchissez pas par la case départ, ne touchez pas 200M
        nb_caseJ2 = 31
    elif carte_tiré==6: #Amende pour excès de vitesse payez 15M
        J2_argent-=15
    elif carte_tiré==7: #Amende pour ivresse payez 25M
        J2_argent-=25
    elif carte_tiré==8: #Votre immeuble et votre prêt rapportent. Vous recevez 150M:
        J2_argent+=150
    elif carte_tiré==9: #Allez a la case départ, recevez 200M
        nb_caseJ2 = 1
        J2_argent+=200
    elif carte_tiré==10: #Erreur de la banque en votre faveur. Recevez 200M:
        J2_argent+=200
    elif carte_tiré==11: #Payez la note du médecin 50M
        J2_argent-=50
    elif carte_tiré==12: #La vente de votre stock vous rapporte 50M'
        J2_argent+=50
    elif carte_tiré==13: #C’est votre anniversaire. Chaque joueur doit vous donner 25M
        J2_argent+=25
        J1_argent-=25
    elif carte_tiré==14: #Payez votre Police d’Assurance 50M
        J2_argent-=50
    elif carte_tiré==15: #Vous avez gagné le deuxième Prix de Beauté. Recevez 10M
        J2_argent+=10
    elif carte_tiré==16: #Vous héritez de 100M
        J2_argent+=100
    elif carte_tiré==17: #Avancez jusqu'a la rue de la paix
        nb_caseJ2 = 40
    elif carte_tiré==18: #Avancez jusqu’au Boulevard de la Villette. Si vous passez par la case départ, recevez 200M
        if nb_caseJ2>12:
            J2_argent+=200
            nb_caseJ2 = 12
        else:
            nb_caseJ2 = 12
    elif carte_tiré==19: #Vous avez été élu president du conseil d’administration. Payez à chaque joueur 50M
        J2_argent-=50
        J1_argent+=50
    affiche_argentJ1(J1_argent)
    affiche_argentJ2(J2_argent)
    affichage_pionJ2(nb_caseJ2)
    roc = rue_or_chance2(nb_caseJ2,rue,chance)
    if roc==1:
       affiche_messJ2('voulez-vous acheter ?')
       bouton2["state"] = tk.DISABLED
       oui2["state"] = tk.NORMAL
       non2["state"] = tk.NORMAL   
        
        
def rue_or_not(nb_case,rue):
    '''
    determine si la case est une rue ou pas.
    créé pour faciliter la fonction rue_or_chance1 et 2.
    '''
    rep = None
    for nombre in rue:
        if nombre==nb_case:
            rep = 1
    return rep

def chance_or_not(nb_case,chance): # determine si la case est une chance ou pas
    rep = None
    for nombre in chance:
        if nombre==nb_case:
            rep = 2
    return rep    

def rue_or_chance1(nb_caseJ1,rue,chance):
    '''
    Permet de terminer la rue sur laquelle le joueur se trouve. 
    les rep possibles sont des numeraux correspondant chacun a un type
    different de case:
        1.Rue
        2.Chance
    renvoit rien dans les autres cas
    '''
    nb_case = nb_caseJ1
    rep = rue_or_not(nb_case,rue)
    if rep!=1:
        rep = chance_or_not(nb_case,chance)
    return rep         

def rue_or_chance2(nb_caseJ2,rue,chance):
    '''
    Permet de terminer la rue sur laquelle le joueur se trouve. 
    les rep possibles sont des numeraux correspondant chacun a un type
    different de case:
        1.Rue
        2.Chance
    renvoit rien dans les autres cas
    '''
    nb_case = nb_caseJ2
    rep = rue_or_not(nb_case,rue)
    if rep!=1:
        rep = chance_or_not(nb_case,chance)
    return rep   
      
def affichage_pionJ1(nb_caseJ1):
    '''
    affiche le pion qui est une image de cercle colorÃ© a l'emplacement auquelle il doit se trouver 
    en fontion de la case sur laquelle le joueur se trouve, les cases Ã©tant numÃ©rautÃ© de 1 Ã  40
    '''
    global oval
    Canevas.delete(oval)
    rep=pos_J1(nb_caseJ1)
    x=rep[0]-15
    y=rep[1]-15
    xx=rep[0]+15
    yy=rep[1]+15
    oval = Canevas.create_oval(x,y,xx,yy,fill='pink')
    
def affichage_pionJ2(nb_caseJ2):
    '''
    affiche le pion qui est une image de cercle colorÃ© a l'emplacement auquelle il doit se trouver 
    en fontion de la case sur laquelle le joueur se trouve, les cases Ã©tant numÃ©rautÃ© de 1 Ã  40
    '''
    global oval2
    Canevas.delete(oval2)
    rep=pos_J2(nb_caseJ2)
    x=rep[0]-15
    y=rep[1]-15
    xx=rep[0]+15
    yy=rep[1]+15
    oval2 = Canevas.create_oval(x,y,xx,yy,fill='light blue')

def messJ1_ajout(message):
    '''
    prend un message et l'ajoute à LA GAUCHE d'une liste nomé L_messJ1 
    affin de pouvoir renvoyer les messages de cette liste au joueur
    '''
    global messageJ1
    Mess=[]
    Mess.append(message)
    for i in range(len(messageJ1)-1):
        Mess.append(messageJ1[i])
    message=Mess
    return Mess

def messJ2_ajout(message):
    '''
    prend un message et l'ajoute à LA GAUCHE d'une liste nomé L_messJ2 
    affin de pouvoir renvoyer les messages de cette liste au joueur
    '''
    global messageJ2
    Mess=[]
    Mess.append(message)
    for i in range(len(messageJ2)-1):
        Mess.append(messageJ2[i])
    message=Mess
    return Mess

def affiche_messJ1(message):
    '''
    affiche les dix messages les plus à gauche de la liste L_messJ1 
    dans le cadre prévu a cette effet (voir plan ecran graphique) 
    '''
    global messageJ1                        
    global y1                               
    Mess=messJ1_ajout(message)
    if y1==345:
        messageJ1=[]
        y1=120
        Canevas.create_rectangle(10,105,205,355,fill='orange')
        Mapolice = Font(family='Liberation Serif', size=15)             
        Canevas.create_text(105,115,text="Messages système",fill="black",font=Mapolice)
    for elmt in Mess:
        Mapolice = Font(family='Liberation Serif', size=10)             
        Canevas.create_text(105,y1+15,text=elmt,fill="black",font=Mapolice)
        y1+=15   

def affiche_messJ2(message):
    '''
    affiche les dix messages les plus à gauche de la liste L_messJ2 
    dans le cadre prévu a cette effet (voir plan ecran graphique) 
    '''
    global messageJ2
    global y2
    Mess=messJ2_ajout(message)
    if y2==345:
        messageJ2=[]
        y2=120
        Canevas.create_rectangle(1000,105,1195,355,fill='light green')
        Mapolice = Font(family='Liberation Serif', size=15)              
        Canevas.create_text(1100,115,text="Messages système",fill="black",font=Mapolice)
    for elmt in Mess:
        Mapolice = Font(family='Liberation Serif', size=10)             
        Canevas.create_text(1097,y2+15,text=elmt,fill="black",font=Mapolice)
        y2+=15

def lancer_dés1():
    '''
    utilise random pour generer 2 nombres aléatoire 
    ATTENTION : le resultat des dés sera utiles pour plusieurs autres fonction,
    il faudra donc les stocker dans deux variables dé1 et dé2
    #creation d'une variable doubleJ1 pour la sortie de prison
    '''
    global doubleJ1
    doubleJ1 = False
    dé1=randint(1,6)
    dé2=randint(1,6)
    score=dé1+dé2
    if dé1==dé2:
        doubleJ1 = True
        affiche_messJ1('Vous avez fait un double !')
    return score

def lancer_dés2():
    global doubleJ2
    doubleJ2 = False
    dé1=randint(1,6)
    dé2=randint(1,6)
    score=dé1+dé2
    if dé1==dé2:
        doubleJ2 = True
        affiche_messJ2('Vous avez fait un double !')
    return score

def Affiche_dés1():
    global bouton1
    global nb_caseJ1
    global dé
    score = lancer_dés1()
    Canevas.create_rectangle(185,65,205,90, fill='red')
    Mapolice = Font(family='Liberation Serif', size=15) 
    Canevas.create_text(195,78,text=score,fill="black",font=Mapolice)
    dé = score
  
def Affiche_dés2():
    global bouton2
    global nb_caseJ2
    global dé
    score = lancer_dés2()
    Canevas.create_rectangle(1000,65,1025,90, fill='red')
    Mapolice = Font(family='Liberation Serif', size=15) 
    Canevas.create_text(1012,78,text=score,fill="black",font=Mapolice)
    dé = score

def pos_J1(nb_caseJ1):
    '''
    calcule les coordonnées sur lequels se trouve le joueur celon la case.
    ATTENTION: il faut que les coordonnées changent si les deux joueurs se 
    retrouvent sur la même case
    '''
    rep = []
    if nb_caseJ1==1:
        x = 910
        y = 750
    elif nb_caseJ1>1 and nb_caseJ1<11:
        y = 750
        x = 950-58*nb_caseJ1 #1020 c la valeur de x+60 pour que moins 60*1 elle donne la bonne case 
    elif nb_caseJ1==11:
        y = 780
        x = 945-58*11 #nb_caseJ1 corespondant
    elif nb_caseJ1>11 and nb_caseJ1<21:
        x = 930-58*11
        y = 787-58*(nb_caseJ1-10)  #870 c la valeur de y+60 pour que moins 60*1 elle donne la bonne case 
    elif nb_caseJ1==21:
        x = 940-58*11
        y = 775-58*11 # 11 pour nb_caseJ1-10 vu que c tj la meme case c tj egal a 11
    elif nb_caseJ1>21 and nb_caseJ1<31:
        y = 775-58*11
        x = 257+58*(nb_caseJ1-20)
    elif nb_caseJ1==31:
        y = 775-58*11
        x = 279+58*11
    elif nb_caseJ1>31 and nb_caseJ1<41:
        x = 279+58*11
        y = 95+58*(nb_caseJ1-30)
    rep.append(x)
    rep.append(y)
    if nb_caseJ1==nb_caseJ2: 
        if nb_caseJ1>0 and nb_caseJ1<11:
            y+=20
        elif nb_caseJ1>11 and nb_caseJ1<22:
            x+=20
        elif nb_caseJ1>21 and nb_caseJ1<32:
            y-=20
        elif nb_caseJ1>31 and nb_caseJ1<41:
            x+=20
        rep = []
        rep.append(x)
        rep.append(y)
    return rep

def pos_J2(nb_caseJ2):
    '''
    calcule les coordonnÃ©es sur lequels se trouve le joueur celon la case.
    ATTENTION: il faut que les coordonnÃ©es changent si les deux joueurs se 
    retrouvent sur la mÃªme case
    '''
    rep = []
    if nb_caseJ2==1:
        x = 910
        y = 750
    elif nb_caseJ2>1 and nb_caseJ2<11:
        y = 750
        x = 950-58*nb_caseJ2 #1020 c la valeur de x+60 pour que moins 60*1 elle donne la bonne case 
    elif nb_caseJ2==11:
        y = 780
        x = 932-58*11 #nb_caseJ2 corespondant
    elif nb_caseJ2>11 and nb_caseJ2<21:
        x = 930-58*11
        y = 787-58*(nb_caseJ2-10)  #870 c la valeur de y+60 pour que moins 60*1 elle donne la bonne case 
    elif nb_caseJ2==21:
        x = 940-58*11
        y = 775-58*11 # 11 pour nb_caseJ2-10 vu que c tj la meme case c tj egal a 11
    elif nb_caseJ2>21 and nb_caseJ2<31:
        y = 775-58*11
        x = 257+58*(nb_caseJ2-20)
    elif nb_caseJ2==31:
        y = 775-58*11
        x = 279+58*11
    elif nb_caseJ2>31 and nb_caseJ2<41:
        x = 279+58*11
        y = 95+58*(nb_caseJ2-30)
    rep.append(x)
    rep.append(y)
    if nb_caseJ1==nb_caseJ2: #declale le joueur si il tombe sur la meme case que l'autre joueur
        if nb_caseJ2>0 and nb_caseJ2<11:
            y-=20
        elif nb_caseJ2==11:
            x-=30
            y-=40
        elif nb_caseJ2>11 and nb_caseJ2<22:
            x-=20
        elif nb_caseJ2>21 and nb_caseJ2<32:
            y+=20
        elif nb_caseJ2>31 and nb_caseJ2<41:
            x-=20
        rep = []
        rep.append(x)
        rep.append(y)
    return rep

def nb_gareJ1(J1_rue): 
    ''' 
    permet de trouver le nombre de gare qu'a J1 pour en calculer le loyer
    '''
    rep = 0
    for rue in J1_rue:
        if rue=='Gare Montparnasse':
            rep += 1
        if rue=='Gare du Nord':
            rep += 1
        if rue=='Gare de lyon':
            rep += 1
        if rue=='Gare Saint-Lazare':
            rep += 1
    return rep

def nb_gareJ2(J2_rue): 
    ''' 
    permet de trouver le nombre de gare qu'a J2 pour en calculer le loyer
    '''
    rep = 0
    for rue in J2_rue:
        if rue=='Gare Montparnasse':
            rep += 1
        if rue=='Gare du Nord':
            rep += 1
        if rue=='Gare de lyon':
            rep += 1
        if rue=='Gare Saint-Lazare':
            rep += 1
    return rep

def nb_companiJ1(): 
    ''' 
    permet de trouver le nombre de companie qu'a J1 pour en calculer le loyer
    '''
    rep = 0
    for rue in J1_rue:
        if rue=="Compagnie de distribution d'électricité":
            rep += 1
        if rue=="Compagnie de distribution des eaux":
            rep += 1
    return rep

def nb_companiJ2(): #permet de trouver le nombre de gare qu'a J1 pour en calculer le loyer
    ''' 
    permet de trouver le nombre de companie qu'a J2 pour en calculer le loyer
    '''
    rep = 0
    for rue in J2_rue:
        if rue=="Compagnie de distribution d'électricité":
            rep += 1
        if rue=="Compagnie de distribution des eaux":
            rep += 1
    return rep

def achat_rueJ1(nb_caseJ1):
    '''
    Doit etre appeler uniquement si le joueur se trouve sur une rue non acheté.
    Elle ajoute au joueur la rue acheter a la liste de ses posession (une liste nomé J1_rue)
    et lui retire le coût de la rue.
    ATTENTION: affin de prevenir d'eventuel bug: prvoir un message si le joueur n'a pas assez d'argent,
    si cette rue est deja acheté ou si ce n'est pas une rue, même si le but est que cette fonction
    ne soit pas appelé dans ces cas.
    '''
    global J1_argent
    global J1_rue
    global plateau
    if J1_argent<plateau[nb_caseJ1-1][4]: #verifie que le joueur a asser d'argent pour payer
        rep = "Vous n'avez pas asser d'argent pour acheter cette rue"
    elif plateau[nb_caseJ1-1][7]!=0:
        rep = 'deja acheté' #provisoire, le but est que cette fonction ne soit pas appeler si la case est deja buy
    else:
        J1_argent-=plateau[nb_caseJ1-1][4] #retire le coup de la rue au joueur
        plateau[nb_caseJ1-1][7] = 1
        J1_rue.append(plateau[nb_caseJ1-1][0]) #ajoute la rue au possesion du joueur
        rep = None
    return rep

def achat_rueJ2(nb_caseJ2):
    '''
    Doit etre appeler uniquement si le joueur se trouve sur une rue non acheté.
    Elle ajoute au joueur la rue acheter a la liste de ses posession (une liste nomé J2_rue)
    et lui retire le coût de la rue.
    ATTENTION: affin de prevenir d'eventuel bug: prevoir un message (different) si le joueur n'a pas assez d'argent,
    si cette rue est deja acheté ou si ce n'est pas une rue, même si le but est que cette fonction
    ne soit pas appelé dans ces cas.
    '''
    global J2_argent
    global J2_rue
    if J2_argent<plateau[nb_caseJ2-1][4]: #verifie que le joueur a asser d'argent pour payer
        rep = "Vous n'avez pas asser d'argent pour acheter cette rue"
    elif plateau[nb_caseJ2-1][7]!=0:
        rep = 'deja acheté' #provisoire, le but est que cette fonction ne soit pas appeler si la case est deja buy
    else:
        J2_argent-=plateau[nb_caseJ2-1][4] #retire le coup de la rue au joueur
        plateau[nb_caseJ2-1][7] = 1
        J2_rue.append(plateau[nb_caseJ2-1][0]) #ajoute la rue au possesion du joueur
        rep = None
    return rep

def J1paye_ou_pas(nb_caseJ1):
    '''
    Fait payer le loyer au joueur 1 pour le joueur 2
    Appeller la fonction rue_or_chance pour verifier 
    si l'on se trouve bien sur une rue(seulement dans un but préventif)
    '''
    global J1_argent
    global J2_argent 
    affiche_messJ1('Vous devez payer au Joueur 2')
    mess = str(plateau[nb_caseJ1-1][3])+' M'
    affiche_messJ1(mess)
    J1_argent-=plateau[nb_caseJ1-1][3]
    J2_argent+=plateau[nb_caseJ1-1][3]
    affiche_argentJ1(J1_argent)
    affiche_argentJ2(J2_argent)

def J2paye_ou_pas(nb_caseJ2):
    '''
    Fait payer le loyer au joueur 1 pour le joueur 2
    '''
    global J1_argent
    global J2_argent 
    affiche_messJ2('Vous devez payer au Joueur 1')
    mess = str(plateau[nb_caseJ2-1][3])+' M'
    affiche_messJ2(mess)
    J1_argent+=plateau[nb_caseJ2-1][3]
    J2_argent-=plateau[nb_caseJ2-1][3]
    affiche_argentJ1(J1_argent)
    affiche_argentJ2(J2_argent)

def go_prisonJ1(nb_caseJ1):
    '''
    envoie J1 en prison si necessaire, c'est a dire dans les cas suivants:
        1.Le joueur tombe sur la case 31
        2.Le joueur fait trois doubles a la suite(crée une variable compt_double)
        3.Si le joueur pioche la carte chance correspondente, cependant rien ne 
        sera à ajouter dans ce cas car la case du joueur aura été changer pour 31.
    ATTENTION : doit lancer la fonction prison() qui bloque toute les posibilités
    d'action du jeueur jusqu'a sa sortie de prison.
    '''
    affiche_messJ1('Vous êtes en prison...')
    nb_caseJ1=0
    affichage_pionJ1(nb_caseJ1)
        
def go_prisonJ2(nb_caseJ2):
    affiche_messJ2('Vous êtes en prison...')
    nb_caseJ2=0
    affichage_pionJ2(nb_caseJ2)
    
def Passer_un_tour():
    '''
    Pour sortir de prison
    '''
    global tours_prison
    tours_prison+=1
  
def ouiJ1():
    global nb_caseJ1
    global J1_rue
    achat_rueJ1(nb_caseJ1)
    affich_propJ1(J1_rue)
    affiche_argentJ1(J1_argent)
    oui1["state"] = tk.DISABLED
    non1["state"] = tk.DISABLED
    bouton2["state"] = tk.NORMAL
   
def nonJ1():
    global nb_caseJ2
    oui1["state"] = tk.DISABLED
    non1["state"] = tk.DISABLED
    bouton1["state"] = tk.DISABLED 
    bouton2["state"] = tk.NORMAL

def nonJ2():
    global nb_caseJ1
    oui2["state"] = tk.DISABLED
    non2["state"] = tk.DISABLED
    bouton1["state"] = tk.NORMAL
    bouton2["state"] = tk.DISABLED
    
def ouiJ2():
    global nb_caseJ2
    global J2_rue
    achat_rueJ2(nb_caseJ2)
    affich_propJ2(J2_rue)
    affiche_argentJ2(J2_argent)
    oui2["state"] = tk.DISABLED
    non2["state"] = tk.DISABLED
    bouton1["state"] = tk.NORMAL
    
def sortie_prison1():
    '''
    Fait sortir le joueur de prison dans les cas suivants:
        1.Le joueur fait un double
        2.le joueur paye 50
        3.le joueur à déja passer 3 tours en prison.
    '''
    global doubleJ1
    global J1_argent
    global nb_caseJ1
    global prison
    global dé
    global reponse
    if doubleJ1==True:
        prison = False
        nb_caseJ1+=11
        nb_caseJ1+=dé
        affichage_pionJ1(nb_caseJ1)
    elif prison>3:
        prison = False
        nb_caseJ1+=11
        nb_caseJ1+=dé
        affichage_pionJ1(nb_caseJ1)
    elif reponse=='oui':
        prison = False
        nb_caseJ1+=11
        nb_caseJ1+=dé
        affichage_pionJ1(nb_caseJ1)
        
def sortie_prison2():
    global doubleJ2
    global J2_argent
    global nb_caseJ2
    global prison
    global dé
    global reponse
    if doubleJ2==True:
        prison = False
        nb_caseJ2+=11
        nb_caseJ2+=dé
        affichage_pionJ2(nb_caseJ2)
    elif prison>3:
        prison = False
        nb_caseJ2+=11
        nb_caseJ2+=dé
        affichage_pionJ2(nb_caseJ2)
    elif reponse=='oui':
        prison = False
        nb_caseJ2+=11
        nb_caseJ2+=dé
        affichage_pionJ2(nb_caseJ2)

def affich_propJ1(J1_rue):
    '''
    Affiche les propriétés de J1, en sachant que ces dernières varie régulièrement.
    '''
    global y_prop
    Mapolice = Font(family='Liberation Serif', size=10)   
    if y_prop==850:
        y_prop=385
    prop = J1_rue[len(J1_rue)-1]
    Canevas.create_text(105,y_prop+15,text=prop ,fill="black",font=Mapolice)
    y_prop+=15

def affich_propJ2(J2_rue):
    '''
    Affiche les propriétés de J2, en sachant que ces dernières varie régulièrement.
    '''
    global y_prop2
    Mapolice = Font(family='Liberation Serif', size=10)   
    if y_prop2==850:
        y_prop2=385
    prop = J2_rue[len(J2_rue)-1]
    Canevas.create_text(1097,y_prop2+15,text=prop ,fill="black",font=Mapolice)
    y_prop2+=15

def recup_parc(nb_caseJ1,nb_caseJ2):
        '''
    Permet de recuperer le parc gratuit si un joueur tombe sur la case 21
    (doit lui reverser l'argent)
        '''
        global J1_argent
        global J2_argent
        global parc_free
        if nb_caseJ1==21:
            affiche_messJ1('Vous étes tombé sur parc gratuit!')
            message = 'Vous recevez'+str(parc_free)+ 'M'
            affiche_messJ1(message)
            J1_argent+=parc_free
            parc_free = 0
        elif nb_caseJ2==21:
            affiche_messJ2('Vous étes tombé sur parc gratuit!')
            message = 'Vous recevez '+str(parc_free)+'M'
            affiche_messJ2(message)
            J2_argent+=parc_free
            parc_free = 0
        affich_parc(parc_free)
    
def affiche_argentJ1(J1_argent):
    '''
    affiche l'argent du jeueur 1 à l'écran, dans le cadre prevu a cet effect(voir fiche prevision interface graphique)
    '''
    global argent
    Canevas.delete(argent)
    Mapolice = Font(family='Liberation Serif', size=15)             #Affiche le titre
    argent = Canevas.create_text(105,42,text=J1_argent ,fill="black",font=Mapolice)
    Canevas.create_text(190,42,text='M' ,fill="black",font=Mapolice)

def affiche_argentJ2(J2_argent):
    '''
    affiche l'argent du jeueur 2 à l'écran, dans le cadre prevu a cet effect(voir fiche prevision interface graphique
    '''
    global argent2
    Canevas.delete(argent2)
    Mapolice = Font(family='Liberation Serif', size=15)             #Affiche le titre
    argent2 = Canevas.create_text(1100,42,text=J2_argent ,fill="black",font=Mapolice)
    Canevas.create_text(1185,42,text='M' ,fill="black",font=Mapolice)
      
def tour(nb_tours):
    '''
    Permet de ternimer les tours de jeu.
    Doit empécher l'autre joueur d'agir.
    '''
    if nb_tours%2==0:
        Canevas.create_rectangle(450,830,750,880,fill='white') 
        Mapolice = Font(family='Liberation Serif', size=20)              
        Canevas.create_text(600,855,text="C'est le tour du joueur 1",fill="black",font=Mapolice) 
        tour = 1    
    elif nb_tours%2==1:
        Canevas.create_rectangle(450,830,750,880,fill='white') 
        Mapolice = Font(family='Liberation Serif', size=20)              
        Canevas.create_text(600,855,text="C'est le tour du joueur 2",fill="black",font=Mapolice) 
        tour = 2
    nb_tours+=1
    return tour

def affich_parc(parc_free):
    '''
    Affiche le montent d'argent du parc gratuit
    '''
    Mapolice = Font(family='Liberation Serif', size=15)
    Canevas.create_rectangle(510,185,670,215,fill='white') # Cadre pour afficher l'argent du parc gratuit
    Canevas.create_text(570,200,text="Parc gratuit :",fill="black", font=Mapolice)
    Canevas.create_text(650,200,text=parc_free,fill="black",font=Mapolice)
    
def affichance(chance):
    '''
    Si le joueur tombe sur une case chance(verification avant d'avoir appeler cette fonction),
    cette fonction doit lui renvoyer le texte de la carte.
    '''
    messchance1 = Canevas.create_rectangle(350,400,860,500,fill='White')
    Mapolice = Font(family='Liberation Serif', size=10)
    messchance2 = Canevas.create_text(600,450,text=chance,fill="black",font=Mapolice) 

def case_chance1(nb_caseJ1):
    '''
    Si la case sur lequel le joueur se trouve est une chance, cette fonction tire au sort une 
    chance dans la variable liste carte_chance et accomplie se que dis la carte.
    '''
    global carte_chance
    global nb_tours
    nb=randint(0,19)
    chance=carte_chance[nb]
    affichance(chance)
    chance_actionJ1(nb)

def case_chance2(nb_caseJ2):
    global carte_chance
    global nb_tours
    nb=randint(0,19)
    chance=carte_chance[nb]
    affichance(chance)
    chance_actionJ2(nb)

def impots_revenu1(nb_caseJ1):
    '''
    si le joueur tombe sur impot sur le revenu lui fait payer la somme carrespondante.
    L'argent doit aller au parc gratuit.
    '''
    global J1_argent
    global parc_free
    J1_argent-=200
    parc_free+=200

def impots_revenu2(nb_caseJ2):
    '''
    si le joueur tombe sur impot sur le revenu lui fait payer la somme carrespondante.
    L'argent doit aller au parc gratuit.
    '''
    global J2_argent
    global parc_free
    J2_argent-=200
    parc_free+=200

def taxe_de_luxe2(nb_caseJ2):
    global J2_argent
    global parc_free
    J2_argent-=100
    parc_free+=100

def taxe_de_luxe1(nb_caseJ1):
    global J1_argent
    global parc_free
    J1_argent-=100
    parc_free+=100
 
def Jeu():
    global nb_caseJ1
    global J1_argent
    global J2_argent
    global rue
    global chance
    Affiche_dés1()
    nb_caseJ1+=dé
    if nb_caseJ1>40:
        nb_caseJ1-=40
        J1_argent+=200
        affiche_argentJ1(J1_argent)
    affichage_pionJ1(nb_caseJ1)
    bouton1["state"] = tk.DISABLED
    roc = rue_or_chance1(nb_caseJ1, rue, chance)
    if roc==1:
        if plateau[nb_caseJ1-1][7]==0:
            affiche_messJ1('voulez-vous acheter ?')
            oui1["state"] = tk.NORMAL
            non1["state"] = tk.NORMAL
        else:
            affiche_messJ1('Rue déja acheté')
            J1paye_ou_pas(nb_caseJ1)
            affiche_argentJ1(J1_argent)
            bouton2["state"] = tk.NORMAL
    if roc==2:
        affiche_messJ1('vous ètes sur une chance!')
        case_chance1(nb_caseJ1)
        affiche_argentJ1(J1_argent)
        bouton2["state"] = tk.NORMAL
    if nb_caseJ1==5:
        affiche_messJ1('payer vos impots sur le revenu')
        affiche_messJ1('-200M')
        impots_revenu1(nb_caseJ1)
        affiche_argentJ1(J1_argent)
        affich_parc(parc_free)
        bouton2["state"] = tk.NORMAL
    if nb_caseJ1==21:
        recup_parc(nb_caseJ1,nb_caseJ2)
        affiche_argentJ1(J1_argent)
        bouton2["state"] = tk.NORMAL
    if nb_caseJ1==39:
        affiche_messJ1('payer vos taxes sur le luxe')
        affiche_messJ1('-100M')
        taxe_de_luxe1(nb_caseJ1)
        affiche_argentJ1(J1_argent)
        affich_parc(parc_free)
        bouton2["state"] = tk.NORMAL
    else:
        bouton2["state"] = tk.NORMAL
         
def Jeu2():
    global nb_caseJ2
    global nb_caseJ1
    global J2_argent
    global J1_argent
    global rue 
    global chance
    Affiche_dés2()
    nb_caseJ2+=dé
    if nb_caseJ2>40:
        nb_caseJ2-=40
        J2_argent+=200
        affiche_argentJ2(J2_argent)
    bouton2["state"] = tk.DISABLED
    affichage_pionJ2(nb_caseJ2)
    roc = rue_or_chance2(nb_caseJ2, rue, chance)
    if roc==1:
        if plateau[nb_caseJ2-1][7]==0:
            affiche_messJ2('voulez-vous acheter ?')
            oui2["state"] = tk.NORMAL
            non2["state"] = tk.NORMAL
        else:
            affiche_messJ2('Rue déja acheté')
            J2paye_ou_pas(nb_caseJ2)
            affiche_argentJ2(J2_argent)
            bouton1["state"] = tk.NORMAL
    if roc==2:
        affiche_messJ2('vous êtes sur une chance!')
        case_chance2(nb_caseJ2)
        bouton1["state"] = tk.NORMAL
    if nb_caseJ2==5:
        affiche_messJ2('payer vos impots sur le revenu')
        affiche_messJ2('-200M')
        impots_revenu2(nb_caseJ2)
        affiche_argentJ2(J2_argent)
        affich_parc(parc_free)
        bouton1["state"] = tk.NORMAL
    if nb_caseJ2==21:
        recup_parc(nb_caseJ1,nb_caseJ2)
        affiche_argentJ2(J2_argent)
        bouton1["state"] = tk.NORMAL
    if nb_caseJ2==39:
        affiche_messJ2('payer vos taxes sur le luxe')
        affiche_messJ2('-100M')
        taxe_de_luxe2(nb_caseJ2)
        affiche_argentJ2(J2_argent)
        affich_parc(parc_free)
        bouton1["state"] = tk.NORMAL
    else:
        bouton1["state"] = tk.NORMAL

       
#########################################################
########## Interface graphique ##########################
#########################################################

Mafenetre = Tk()
Mafenetre.title("Monopoly")
Canevas = Canvas(Mafenetre,width=1200,height=900,bg ='white')
Canevas.pack()
Canevas.create_rectangle(10,30,205,55,fill='orange') #Joueur 1
Canevas.create_rectangle(10,105,205,355,fill='orange') #Messages système
Canevas.create_rectangle(10,370,205,870,fill='orange') #Vos propriétés
Canevas.create_rectangle(215,50,990,850,fill='white') #Plateau
Canevas.create_rectangle(1000,30,1195,55,fill='light green') #Joueur 2
Canevas.create_rectangle(1000,105,1195,355,fill='light green') #Message système
Canevas.create_rectangle(1000,370,1195,870,fill='light green') #Vos propriétés
Canevas.create_rectangle(450,830,750,880,fill='white') #Message qui affiche le joueur qui joue

fichier=PhotoImage(file='plateau.gif')
img=Canevas.create_image(605,440,image=fichier)

Mapolice = Font(family='Liberation Serif', size=35)             #Affiche le titre
Canevas.create_text(600,20,text="Monopoly",fill="black",font=Mapolice)
Mapolice = Font(family='Liberation Serif', size=20)             #Affiche le Joueur 1
Canevas.create_text(105,17,text="Joueur 1",fill="black",font=Mapolice)
Mapolice = Font(family='Liberation Serif', size=15)             #Affiche les messages
Canevas.create_text(105,115,text="Messages système",fill="black",font=Mapolice)
Mapolice = Font(family='Liberation Serif', size=15)             #Affiche les propriétés
Canevas.create_text(105,385,text="Vos propriétés :",fill="black",font=Mapolice)
Mapolice = Font(family='Liberation Serif', size=20)             #Affiche le Joueur 2
Canevas.create_text(1100,17,text="Joueur 2",fill="black",font=Mapolice)
Mapolice = Font(family='Liberation Serif', size=15)              #Affiche les messages
Canevas.create_text(1100,115,text="Messages système",fill="black",font=Mapolice)
Mapolice = Font(family='Liberation Serif', size=15)              #Affiche les propriétés
Canevas.create_text(1100,385,text="Vos propriétés :",fill="black",font=Mapolice) 
Mapolice = Font(family='Liberation Serif', size=15) 

oval = Canevas.create_oval(895,725,925,755,fill='pink')
oval2 = Canevas.create_oval(895,760,925,790, fill='light blue')
tours=tour(nb_tours)

Mapolice = Font(family='Liberation Serif', size=15)             
argent = Canevas.create_text(105,42,text='1500' ,fill="black",font=Mapolice)
Canevas.create_text(190,42,text='M' ,fill="black",font=Mapolice)

Mapolice = Font(family='Liberation Serif', size=15)             
argent2 = Canevas.create_text(1100,42,text=1500 ,fill="black",font=Mapolice)
Canevas.create_text(1185,42,text='M' ,fill="black",font=Mapolice)
###########################################################
########### Receptionnaire d'évènement ####################
###########################################################

bouton1 = Button(Mafenetre,text="Lancer de dés", bg='light green', fg='white', command=Jeu)
bouton1.place(x=pbt1_dés,y=65)

oui1 = Button(Mafenetre,text=" oui ", bg='light green', fg='white', command=ouiJ1)
oui1.place(x=pbt1_oui,y=65)
non1 = Button(Mafenetre,text=" non ", bg='light green', fg='white', command=nonJ1)
non1.place(x=pbt1_non,y=65)

bouton2 = Button(Mafenetre,text="Lancer de dés", bg='orange', fg='white', command=Jeu2)
bouton2.place(x=pbt2_dés,y=65)

oui2 = Button(Mafenetre,text=" oui ", bg='orange', fg='white', command=ouiJ2)
oui2.place(x=pbt2_oui,y=65) 
non2 = Button(Mafenetre,text=" non ", bg='orange', fg='white', command=nonJ2)
non2.place(x=pbt2_non,y=65)

oui1["state"] = tk.DISABLED
non1["state"] = tk.DISABLED  
oui2["state"] = tk.DISABLED
non2["state"] = tk.DISABLED  
bouton2["state"] = tk.DISABLED
##########################################################
############# Programme principal ########################
##########################################################

tours=tour(nb_tours)
affichage_pionJ1(nb_caseJ1)
affichage_pionJ2(nb_caseJ2)
affich_parc(parc_free)
 
###################### FIN ###############################  
Mafenetre.mainloop()
"""
while J1_argent>=0 and J2_argent>=0:
    if tour(nb_tours)==1:
        dé = 0
        if prison==True:
            reponse = 'non'
            affiche_messJ1('50M la sortie de prison. O=oui N=non')
            if reponse=='oui':
                dé=lancer_dés1()
                time.sleep(2)
                nb_caseJ1+=dé
                affichage_pionJ1(nb_caseJ1)
                time.sleep(2)
                J1paye_ou_pas(nb_caseJ1)
                type_case = rue_or_chance1(nb_caseJ1,rue,chance)
                if type_case==1:
                    time.sleep(2)
                    if rue[nb_caseJ1][7]==0:
                        reponse = 'non'
                        affiche_messJ1('voulez vous acheter la rue?')
                        time.sleep(1)
                        affiche_messJ1('taper O pour oui, N pour non')
                        time.sleep(5)
                        if reponse=='oui':
                            achat_rueJ1(nb_caseJ1)
                            time.sleep(2)
                            affich_propJ1()
                    elif type_case==2:
                        case_chance1(nb_caseJ1)
                    elif nb_caseJ1==5:
                        affiche_messJ1('payez cet impôt')
                        impots_revenu1(nb_caseJ1)
                    elif nb_caseJ1==21:
                        affiche_messJ1("vous recevez l'argent du parc")
                        time.sleep(2)
                        recup_parc()
                    elif nb_caseJ1==31:
                        affiche_messJ1('allez en prison')
                        time.sleep(2)
                        go_prisonJ1(nb_caseJ1)
                    elif nb_caseJ1==39:
                        affiche_messJ1('payez cette taxe')
                        time.slepp(2)
                        taxe_de_luxe1(nb_caseJ1)
                        nb_tours+=1
            else:
                time.sleep(1)
                affiche_messJ1('sortie automatique au bout de trois tours')
                nb_tours+=1
        else: #le joueur n'est pas en prison
            dé=lancer_dés1()
            time.sleep(2)
            nb_caseJ1+=dé
            #affichage_pionJ1(nb_caseJ1)
            time.sleep(2)
            J1paye_ou_pas(nb_caseJ1)
            type_case = rue_or_chance1(nb_caseJ1,rue,chance)
        if type_case==1:
            time.sleep(2)
            '''
            if rue[nb_caseJ1][7]==0: #######################################
                reponse = 'non'
                affiche_messJ1('voulez vous acheter la rue?')
                time.sleep(1)
                affiche_messJ1('taper O pour oui, N pour non')
                if reponse=='oui':
                    achat_rueJ1(nb_caseJ1)
                    time.sleep(2)
                    affich_propJ1()
            '''
        elif type_case==2:
            case_chance1(nb_caseJ1)
        elif nb_caseJ1==5:
            affiche_messJ1('payez cet impôt')
            impots_revenu1(nb_caseJ1)
        elif nb_caseJ1==21:
            affiche_messJ1("vous recevez l'argent du parc")
            time.sleep(2)
            recup_parc()
        elif nb_caseJ1==31:
            affiche_messJ1('allez en prison')
            time.sleep(2)
            go_prisonJ1(nb_caseJ1)
        elif nb_caseJ1==39:
            affiche_messJ1('payez cette taxe')
            time.slepp(2)
            taxe_de_luxe1(nb_caseJ1)
            nb_tours+=1
    
    elif nb_tours%2==1:
        if prison==True:
            reponse = 'non'
            affiche_messJ2('50M la sortie de prison. O=oui N=non')
            if reponse=='oui':
                dé=Lancer_dés2()
                time.sleep(2)
                nb_caseJ2+=dé
                affichage_pionJ2(nb_caseJ2)
                time.sleep(2)
                J2paye_ou_pas(nb_caseJ2)
                type_case = rue_or_chance2(nb_caseJ2,rue,chance)
                if type_case==1:
                    time.sleep(2)
                    if rue[nb_caseJ2][7]==0:
                        reponse = 'non'
                        affiche_messJ2('voulez vous acheter la rue?')
                        time.sleep(1)
                        affiche_messJ2('taper O pour oui, N pour non')
                        time.sleep(5)
                    if reponse=='oui':
                        achat_rueJ2(nb_caseJ2)
                        time.sleep(2)
                        affich_propJ2()
                elif type_case==2:
                    case_chance1(nb_caseJ2)
                elif nb_caseJ1==5:
                    affiche_messJ2('payez cet impôt')
                    impots_revenu1(nb_caseJ2)
                elif nb_caseJ2==21:
                    affiche_messJ2("vous recevez l'argent du parc")
                    time.sleep(2)
                    recup_parc()
                elif nb_caseJ2==31:
                    affiche_messJ2('allez en prison')
                    time.sleep(2)
                    go_prisonJ2(nb_caseJ2)
                elif nb_caseJ2==39:
                    affiche_messJ2('payez cette taxe')
                    time.sleep(2)
                    taxe_de_luxe2(nb_caseJ2)
                    nb_tours+=1
            else:
                time.sleep(1)
                affiche_messJ2('sortie automatique au bout de trois tours')
        else:
            dé=Lancer_dés2()
            time.sleep(2)
            nb_caseJ2+=dé
            affichage_pionJ2(nb_caseJ2)
            time.sleep(2)
            J1paye_ou_pas(nb_caseJ2)
            type_case = rue_or_chance2(nb_caseJ2,rue,chance)
            if type_case==1:
                time.sleep(2)
                if rue[nb_caseJ2][7]==0:
                    reponse = 'non'
                    affiche_messJ2('voulez vous acheter la rue?')
                    time.sleep(1)
                    affiche_messJ2('taper O pour oui, N pour non')
                    if reponse=='oui':
                        achat_rueJ2(nb_caseJ2)
                        time.sleep(2)
                        affich_propJ2()
            elif type_case==2:
                case_chance2(nb_caseJ2)
            elif nb_caseJ2==5:
                affiche_messJ2('payez cet impôt')
                impots_revenu2(nb_caseJ2)
            elif nb_caseJ2==21:
                affiche_messJ2("vous recevez l'argent du parc")
                time.sleep(2)
                recup_parc()
            elif nb_caseJ2==31:
                affiche_messJ2('allez en prison')
                time.sleep(2)
                go_prisonJ2(nb_caseJ2)
            elif nb_caseJ2==39:
                affiche_messJ2('payez cette taxe')
                time.sleep(2)
                taxe_de_luxe2(nb_caseJ2)
"""
